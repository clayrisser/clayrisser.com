---
# preview details
title: Gnome Gobject
category: Development
category_slug: development
image: images/portfolio/gnome-gobject.png
short_description: TypeScript bindings for GNOME GObject.

# full details
live_preview: https://github.com/clayrisser/gnome-gobject
full_image: images/portfolio/gnome-gobject.png
info:
  - label: Year
    value: 2019-Present
  - label: Technology
    value: Node.js

description1:
  enable: 1
  title: Project Goal
  text1: "<p>The <b>gnome-gobject</b> project aims to provide TypeScript bindings for GNOME GObject, enabling developers to access the full GObject API from TypeScript. It facilitates the development of GNOME applications using TypeScript with the powerful GObject framework.</p>"

description2:
  enable: 1
  title: Project Result
  text1: "<p><b>gnome-gobject</b> offers comprehensive TypeScript bindings for GNOME GObject. By utilizing these bindings, developers can leverage the entire GObject API from TypeScript, enhancing the development experience of GNOME applications. The project has been highly appreciated for its contribution to enabling GObject-based development with TypeScript.</p>"

gallery:
  enable: 0
  images:
    - images/works/work5.jpeg
    - images/blog/blog8.jpg
    - images/blog/blog6.jpg

video:
  enable: 0
  poster: images/blog/blog9.jpg
  id: Gu6z6kIukgg

---
