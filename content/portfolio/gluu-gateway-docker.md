---
# preview details
title: Gluu Gateway Docker
category: DevOps
category_slug: devops
image: images/portfolio/gluu-gateway-docker.png
short_description: Run gluu-gateway from a Docker container.

# full details
live_preview: https://github.com/clayrisser/gluu-gateway-docker
full_image: images/portfolio/gluu-gateway-docker.png
info:
  - label: Year
    value: 2019-Present
  - label: Technology
    value: Docker, gluu-gateway

description1:
  enable: 1
  title: Project Goal
  text1: "<p>The <b>gluu-gateway-docker</b> project provides a convenient way to run the gluu-gateway from a Docker container. It allows users to easily deploy and manage the gluu-gateway in a containerized environment, simplifying the setup and configuration process.</p>"

description2:
  enable: 1
  title: Project Result
  text1: "<p><b>gluu-gateway-docker</b> offers a streamlined solution for running the gluu-gateway, providing all the benefits of containerization. It simplifies deployment and enables efficient management of the gluu-gateway in various environments. The project has been well-received by users and has contributed to the adoption and usage of the gluu-gateway.</p>"

gallery:
  enable: 0
  images:
    - images/works/work5.jpeg
    - images/blog/blog8.jpg
    - images/blog/blog6.jpg

video:
  enable: 0
  poster: images/blog/blog9.jpg
  id: Gu6z6kIukgg
---
