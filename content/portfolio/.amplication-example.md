---
# preview details
title: Amplication Example
category: React
category_slug: react
image: images/portfolio/amplication-example.png
short_description: An example project showcasing the usage of amplication.

# full details
live_preview: https://github.com/clayrisser/amplication-example
full_image: images/portfolio/amplication-example.png
info:
  - label: Year
    value: 2022-Present
  - label: Technology
    value: React, amplication

description1:
  enable: 1
  title: Project Goal
  text1: "<p>The <b>amplication-example</b> project aims to demonstrate the usage of amplication, a low-code development platform. It showcases the integration of React components and the creation of a simple application using amplication's features.</p>"

description2:
  enable: 1
  title: Project Result
  text1: "<p>The <b>amplication-example</b> project serves as a starting point for developers interested in exploring amplication. It provides a basic React application with a user interface and integration with amplication's data model and features. It has been widely used as a reference and learning resource for amplication development.</p>"

gallery:
  enable: 0
  images:
    - images/works/work5.jpeg
    - images/blog/blog8.jpg
    - images/blog/blog6.jpg

video:
  enable: 0
  poster: images/blog/blog9.jpg
  id: Gu6z6kIukgg
---
