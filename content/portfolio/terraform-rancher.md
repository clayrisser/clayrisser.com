---
title: Terraform Rancher
category: DevOps
category_slug: devops
image: images/portfolio/terraform-rancher.png
short_description: Initialize Rancher with Terraform.

# full details
live_preview: https://github.com/clayrisser/terraform-rancher
full_image: images/portfolio/terraform-rancher.png
info:
  - label: Year
    value: 2018-Present
  - label: Technology
    value: AWS, Terraform

description1:
  enable: 1
  title: Project Goal
  text1: "<p>The <b>Terraform Rancher</b> project aims to simplify the process of initializing Rancher using Terraform. It provides an automated solution for setting up Rancher servers, registering servers with CloudFlare, and adding Let's Encrypt certificates to Rancher. The project also supports AutoSpotting and allows tagging Rancher hosts with spot or dedicated lifestyle.</p>
  <p>By creating this project, I showcase my expertise in working with AWS and Terraform, as well as my ability to develop efficient and scalable infrastructure automation solutions.</p>"

gallery:
  enable: 0
  images:
    - images/works/work5.jpeg
    - images/blog/blog8.jpg
    - images/blog/blog6.jpg

description2:
  enable: 1
  title: Project Result
  text1: "<p>The <b>Terraform Rancher</b> project has received positive feedback from the DevOps community and has proven to be a valuable tool for simplifying Rancher setup and configuration. It reflects my commitment to providing practical solutions that enhance infrastructure automation and streamline deployment processes.</p>
  <p>By actively maintaining and improving this project, I continue to demonstrate my expertise in AWS and Terraform, and my dedication to supporting the DevOps community.</p>"

video:
  enable: 0
  poster: images/blog/blog9.jpg
  id: Gu6z6kIukgg

---
