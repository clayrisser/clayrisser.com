---
# preview details
title: Kubenuke
category: Configuration
category_slug: configuration
image: images/portfolio/kubenuke.png
short_description: A tool for nuking stuck Kubernetes namespaces, following the termination process outlined by IBM.

# full details
live_preview: https://github.com/clayrisser/kubenuke
full_image: images/portfolio/kubenuke.png
info:
  - label: Year
    value: 2020-Present
  - label: Technology
    value: Node.js

description1:
  enable: 1
  title: Project Goal
  text1: "<p>The <b>kubenuke</b> project aims to provide a solution for removing stuck Kubernetes namespaces. It follows the process outlined by IBM to terminate namespaces that are not terminating properly, helping to resolve pesky issues caused by stuck namespaces.</p>"

description2:
  enable: 1
  title: Project Result
  text1: "<p><b>kubenuke</b> offers a convenient tool for effectively removing stuck Kubernetes namespaces. By following the termination process recommended by IBM, it addresses challenges associated with namespaces that fail to terminate. The project has been appreciated for its contribution to resolving namespace-related issues.</p>"

gallery:
  enable: 0
  images:
    - images/works/work5.jpeg
    - images/blog/blog8.jpg
    - images/blog/blog6.jpg

video:
  enable: 0
  poster: images/blog/blog9.jpg
  id: Gu6z6kIukgg

---
