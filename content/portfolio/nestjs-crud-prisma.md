---
# preview details
title: NestJs Crud Prisma
category: React
category_slug: react
image: images/portfolio/nestjs-crud-prisma.png
short_description: A CRUD library for building RESTful APIs with NestJS and Prisma.

# full details
live_preview: https://github.com/clayrisser/nestjs-crud-prisma
full_image: images/portfolio/nestjs-crud-prisma.png
info:
  - label: Year
    value: 2020-Present
  - label: Technology
    value: NestJS, Prisma

description1:
  enable: 1
  title: Project Goal
  text1: "<p>The <b>nestjs-crud-prisma</b> project aims to provide a convenient and efficient way to develop RESTful APIs using NestJS and Prisma. It offers a CRUD library that simplifies the creation of CRUD endpoints and integrates seamlessly with Prisma for data access and manipulation.</p>
  <p>By utilizing this library, I demonstrate my proficiency in NestJS and Prisma and showcase my ability to build robust and scalable backend applications.</p>"

gallery:
  enable: 0
  images:
    - images/works/work5.jpeg
    - images/blog/blog8.jpg
    - images/blog/blog6.jpg

description2:
  enable: 1
  title: Project Result
  text1: "<p>The <b>nestjs-crud-prisma</b> project has proven to be a valuable tool for developers building RESTful APIs with NestJS and Prisma. It provides a consistent and intuitive approach to handling CRUD operations, reducing development time and effort.</p>
  <p>This project contributes to my portfolio of backend development projects, showcasing my expertise in using NestJS and Prisma to create efficient and scalable APIs.</p>"

video:
  enable: 0
  poster: images/blog/blog9.jpg
  id: Gu6z6kIukgg
---
