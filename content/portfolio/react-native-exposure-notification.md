---
# preview details
title: React Native Exposure Notification
category: React
category_slug: react
image: images/portfolio/react-native-exposure-notification.png
short_description: Access the COVID-19 exposure notification API from React Native.

# full details
live_preview: https://github.com/clayrisser/react-native-exposure-notification
full_image: images/portfolio/react-native-exposure-notification.png
info:
  - label: Year
    value: 2020-Present
  - label: Technology
    value: React Native, COVID-19 Exposure Notification API

description1:
  enable: 1
  title: Project Goal
  text1: "<p>The <b>React Native Exposure Notification</b> project aims to provide seamless integration of React Native apps with the iOS and Android Exposure Notification API. By leveraging this API, developers can help prevent the spread of COVID-19 by building contact tracing and exposure notification features into their apps.</p>"

description2:
  enable: 1
  title: Project Result
  text1: "<p>The <b>React Native Exposure Notification</b> library allows developers to access the COVID-19 Exposure Notification API from their React Native apps. It simplifies the process of setting up and utilizing the API, enabling faster development of contact tracing and exposure notification features. Collaborating on this project will help bring your app to market quickly and efficiently.</p>"

gallery:
  enable: 0
  images:
    - images/works/work5.jpeg
    - images/blog/blog8.jpg
    - images/blog/blog6.jpg

video:
  enable: 0
  poster: images/blog/blog9.jpg
  id: Gu6z6kIukgg

---
