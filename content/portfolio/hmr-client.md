---
# preview details
title: HMR Client
category: Nodejs
category_slug: nodejs
image: images/portfolio/hmr-client.png
short_description: A client for communicating with hot module replacement (HMR).

# full details
live_preview: https://github.com/clayrisser/hmr-client
full_image: images/portfolio/hmr-client.png
info:
  - label: Year
    value: 2018-Present
  - label: Technology
    value: Node.js

description1:
  enable: 1
  title: Project Goal
  text1: "<p>The <b>HMR Client</b> project aims to provide a client for communicating with hot module replacement (HMR) in Node.js applications. It enables developers to receive HMR events and handle them accordingly.</p>"

description2:
  enable: 1
  title: Project Result
  text1: "<p><b>HMR Client</b> simplifies the process of interacting with hot module replacement (HMR) in Node.js projects. It provides event listeners for connected, close, hash, still-ok, content-changed, warnings, errors, and ok events. The project has gained popularity for its contribution to enhancing the HMR workflow in Node.js development.</p>"

gallery:
  enable: 0
  images:
    - images/works/work5.jpeg
    - images/blog/blog8.jpg
    - images/blog/blog6.jpg

video:
  enable: 0
  poster: images/blog/blog9.jpg
  id: Gu6z6kIukgg
---
