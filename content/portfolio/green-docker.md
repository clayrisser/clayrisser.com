---
# preview details
title: Green Docker
category: DevOps
category_slug: devops
image: images/portfolio/green-docker.png
short_description: Make the most efficient Docker image using best practices to reduce greenhouse emissions.

# full details
live_preview: https://github.com/clayrisser/green-docker
full_image: images/portfolio/green-docker.png
info:
  - label: Year
    value: 2017-Present
  - label: Technology
    value: Docker

description1:
  enable: 1
  title: Project Goal
  text1: "<p>The <b>Green Docker</b> project focuses on creating highly efficient Docker images by following best practices. The goal is to reduce greenhouse emissions by optimizing code and adopting efficient Docker image creation techniques.</p>"

description2:
  enable: 1
  title: Project Result
  text1: "<p><b>Green Docker</b> promotes sustainable development practices by encouraging the creation of Docker images that minimize resource consumption and maximize efficiency. It has gained recognition for its contribution to reducing environmental impact and promoting eco-friendly coding practices.</p>"

gallery:
  enable: 0
  images:
    - images/works/work5.jpeg
    - images/blog/blog8.jpg
    - images/blog/blog6.jpg

video:
  enable: 0
  poster: images/blog/blog9.jpg
  id: Gu6z6kIukgg
---
