---
# preview details
title: Gnome Glib
category: Development
category_slug: development
image: images/portfolio/gnome-glib.png
short_description: TypeScript bindings for GNOME GLib.

# full details
live_preview: https://github.com/clayrisser/gnome-glib
full_image: images/portfolio/gnome-glib.png
info:
  - label: Year
    value: 2019-Present
  - label: Technology
    value: Node.js

description1:
  enable: 1
  title: Project Goal
  text1: "<p>The <b>gnome-glib</b> project aims to provide TypeScript bindings for GNOME GLib, allowing developers to access the full GLib API from TypeScript. It enables the development of GNOME applications using TypeScript with the power and functionality of the GLib library.</p>"

description2:
  enable: 1
  title: Project Result
  text1: "<p><b>gnome-glib</b> offers comprehensive TypeScript bindings for GNOME GLib. By leveraging these bindings, developers can access the entire GLib API from TypeScript, facilitating the development of GNOME applications. The project has been well-received for its contribution to enhancing the development experience of GNOME applications with TypeScript.</p>"

gallery:
  enable: 0
  images:
    - images/works/work5.jpeg
    - images/blog/blog8.jpg
    - images/blog/blog6.jpg

video:
  enable: 0
  poster: images/blog/blog9.jpg
  id: Gu6z6kIukgg

---
