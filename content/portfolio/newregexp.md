---
# preview details
title: NewRegExp
category: Development
category_slug: development
image: images/portfolio/newregexp.png
short_description: Dynamically create regular expressions from strings.

# full details
live_preview: https://github.com/clayrisser/newregexp
full_image: images/portfolio/newregexp.png
info:
 - label: Year
   value: 2018-Present
 - label: Technology
   value: nodejs

description1:
  enable: 1
  title: Project Goal
  text1: "<p>The <b>NewRegExp</b> project aims to simplify the process of dynamically creating regular expressions from strings. It provides a convenient API that works with the native JavaScript RegExp API and understands regex flags. This tool is particularly useful for developers who need to generate regular expressions programmatically based on user input or configuration files.</p>

  <p>By creating this project, I showcase my expertise in working with regular expressions and my ability to develop practical tools that enhance developer productivity.</p>"

gallery:
  enable: 0
  images:
    - images/works/work5.jpeg
    - images/blog/blog8.jpg
    - images/blog/blog6.jpg
    
description2:
  enable: 1
  title: Project Result
  text1: "<p>The <b>NewRegExp</b> project has gained popularity among developers and has proven to be a valuable utility for dynamically generating regular expressions. It reflects my commitment to providing efficient and reliable tools that simplify complex tasks in JavaScript development.</p>
  <p>By actively maintaining and improving this project, I continue to demonstrate my expertise in working with regular expressions and my dedication to supporting the developer community.</p>"

video:
  enable: 0
  poster: images/blog/blog9.jpg
  id: Gu6z6kIukgg

---
