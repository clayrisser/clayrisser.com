---
# preview details
title: NestJS Prisma Module
category: React
category_slug: react
image: images/portfolio/nestjs-prisma-module.png
short_description: NestJS Prisma module.

# full details
live_preview: https://github.com/clayrisser/nestjs-prisma-module
full_image: images/portfolio/nestjs-prisma-module.png
info:
  - label: Year
    value: 2020-Present
  - label: Technology
    value: Node.js

description1:
  enable: 1
  title: Project Goal
  text1: "<p>The <b>NestJS Prisma Module</b> project aims to provide a module for integrating Prisma with NestJS, a popular Node.js framework. It simplifies the usage of Prisma as an ORM (Object-Relational Mapping) tool within NestJS applications.</p>"

description2:
  enable: 1
  title: Project Result
  text1: "<p><b>NestJS Prisma Module</b> offers an efficient way to incorporate Prisma into NestJS projects. It enables developers to seamlessly integrate Prisma's capabilities for database operations within the NestJS framework. The project has gained popularity for its contribution to enhancing the development experience with Prisma and NestJS.</p>"

gallery:
  enable: 0
  images:
    - images/works/work5.jpeg
    - images/blog/blog8.jpg
    - images/blog/blog6.jpg

video:
  enable: 0
  poster: images/blog/blog9.jpg
  id: Gu6z6kIukgg
---
