---
# preview details
title: Generator Sphinx
category: Development
category_slug: development
image: images/portfolio/generator-sphinx.png
short_description: A Yeoman generator for Sphinx, a documentation tool, allowing easy creation of HTML, PDF, and other document formats.

# full details
live_preview: https://github.com/clayrisser/generator-sphinx
full_image: images/portfolio/generator-sphinx.png
info:
  - label: Year
    value: 2018-Present
  - label: Technology
    value: NodeJS, GNU Make, Python 3, Virtualenv, Latexmk

description1:
  enable: 1
  title: Project Goal
  text1: "<p>The <b>Generator Sphinx</b> project aims to simplify the creation of documentation using the Sphinx tool. It provides a Yeoman generator that automates the setup process and offers features like building HTML, PDF, man pages, and choosing from multiple themes.</p>"

description2:
  enable: 1
  title: Project Result
  text1: "<p><b>Generator Sphinx</b> is a convenient tool for developers and writers who want to generate documentation effortlessly. It supports various document formats and incorporates markdown, math expressions, and multiple Sphinx domains. The project has been widely adopted and appreciated for its contributions to the documentation process.</p>"

gallery:
  enable: 1
  images:
    - images/works/work5.jpeg
    - images/blog/blog8.jpg
    - images/blog/blog6.jpg

video:
  enable: 0
  poster: images/blog/blog9.jpg
  id: Gu6z6kIukgg
---
