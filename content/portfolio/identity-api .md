---
# preview details
title: Identity API
category: Development
category_slug: development
image: images/portfolio/identity-api.png
short_description: An identity API built with LoopBack.

# full details
live_preview: https://github.com/clayrisser/identity-api
full_image: images/portfolio/identity-api.png
info:
  - label: Year
    value: 2019-Present
  - label: Technology
    value: Node.js, LoopBack 4

description1:
  enable: 1
  title: Project Goal
  text1: "<p>The <b>Identity API</b> project aims to provide a robust identity API built with LoopBack 4. It offers features such as user creation and login, serving as a foundation for authentication and authorization in web applications. By utilizing TypeScript and LoopBack 4, the project emphasizes strong typing and maintainable code, making it an efficient and scalable solution for managing user identities.</p>"

gallery:
  enable: 0
  images:
    - images/works/work5.jpeg
    - images/blog/blog8.jpg
    - images/blog/blog6.jpg

description2:
  enable: 1
  title: Project Result
  text1: "<p>The <b>Identity API</b> project has gained popularity for its simplicity and reliability in managing user identities. It has been well-received by the developer community and serves as a valuable resource for implementing authentication and authorization in web applications.By actively maintaining and improving this project, I showcase my expertise in Node.js and LoopBack 4 development, while contributing to the advancement of secure web application development.</p>"

video:
  enable: 0
  poster: images/blog/blog9.jpg
  id: Gu6z6kIukgg
---
