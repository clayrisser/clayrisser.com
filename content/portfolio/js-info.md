---
# preview details
title: JS Info
category: JavaScript
category_slug: javascript
image: images/portfolio/js-info.png
short_description: Get system information about your JavaScript runtime engine.

# full details
live_preview: https://github.com/clayrisser/js-info
full_image: images/portfolio/js-info.png
info:
  - label: Year
    value: 2018-Present
  - label: Technology
    value: Node.js

description1:
  enable: 1
  title: Project Goal
  text1: "<p>The <b>JS Info</b> project aims to provide a utility to retrieve system information about your JavaScript runtime engine. It offers a simple way to obtain runtime details, environment specifics, and JavaScript engine information.</p>"

description2:
  enable: 1
  title: Project Result
  text1: "<p><b>JS Info</b> enables developers to gather useful system information about their JavaScript environment. It is completely cross-platform and supports runtime detection, determining if a device is mobile, retrieving operating system details, JavaScript engine identification, environment detection, and browser identification. The project has gained popularity for its versatility and contribution to retrieving runtime-specific information in JavaScript projects.</p>"

gallery:
  enable: 0
  images:
    - images/works/work5.jpeg
    - images/blog/blog8.jpg
    - images/blog/blog6.jpg

video:
  enable: 0
  poster: images/blog/blog9.jpg
  id: Gu6z6kIukgg
---
