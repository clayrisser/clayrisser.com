---
# preview details
title: Junix
category: Development
category_slug: development
image: images/portfolio/junix.png
short_description: An open-source Unix OS built from scratch.

# full details
live_preview: https://github.com/clayrisser/junix
full_image: images/portfolio/junix.png
info:
  - label: Year
    value: 2018-Present
  - label: Technology
    value: Assembly

description1:
  enable: 1
  title: Project Goal
  text1: "<p>The <b>Junix</b> project aims to develop an open-source Unix-like operating system built entirely from scratch. It is an ambitious project that focuses on implementing the core functionalities of an operating system using assembly language.</p>"

description2:
  enable: 1
  title: Project Result
  text1: "<p><b>Junix</b> is an ongoing project that currently provides a basic framework of an operating system. Although it is still in the early stages, Junix demonstrates the dedication to building an OS from scratch. The project has gained attention for its commitment to low-level system development and the exploration of operating system internals.</p>"

gallery:
  enable: 0
  images:
    - images/works/work5.jpeg
    - images/blog/blog8.jpg
    - images/blog/blog6.jpg

video:
  enable: 0
  poster: images/blog/blog9.jpg
  id: Gu6z6kIukgg
---
