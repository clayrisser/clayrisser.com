---
# Preview Details
title: mkLink
category: Development
category_slug: development
image: images/portfolio/mklink.png
short_description: Make symbolic links, hard links, and directory junctions from the context menu.

# Full Details
live_preview: https://github.com/clayrisser/mklink
full_image: images/portfolio/mklink.png
info:
  - label: Year
    value: 2016-Present
  - label: Technology
    value: Symbolic Links, Hard Links, Directory Junctions

description1:
  enable: 1
  title: Project Goal
  text1: "<p>The <b>mkLink</b> project was created to simplify the process of creating symbolic links, hard links, and directory junctions. It provides a GUI interface and integrates with the context menu, making it easier for users to create advanced symbolic links without relying on the command prompt.</p>
  <p>This project showcases my ability to develop user-friendly tools and improve developer productivity by simplifying complex tasks.</p>"

gallery:
  enable: 0
  images:
    - images/works/work5.jpeg
    - images/blog/blog8.jpg
    - images/blog/blog6.jpg

description2:
  enable: 1
  title: Project Result
  text1: "<p>The <b>mkLink</b> project has been well-received by users who frequently work with symbolic links. It has provided a convenient and accessible solution for creating various types of links, improving the overall efficiency of file management tasks.</p>
  <p>By actively maintaining and enhancing this project, I continue to demonstrate my expertise in DevOps and my commitment to delivering practical tools that benefit the developer community.</p>"

video:
  enable: 0
  poster: images/blog/blog9.jpg
  id: Gu6z6kIukgg

---
