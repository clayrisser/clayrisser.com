---
# preview details
title: Mergepath
category: Development
category_slug: development
image: images/portfolio/mergepath.png
short_description: A GoLang library to merge directories.

# full details
live_preview: https://github.com/clayrisser/mergepath
full_image: images/portfolio/mergepath.png
info:
  - label: Year
    value: 2019-Present
  - label: Technology
    value: GoLang

description1:
  enable: 1
  title: Project Goal
  text1: "<p>The <b>mergepath</b> project aims to provide a GoLang library for merging directories. It allows developers to merge directories, handling file links and resolving conflicts when overwriting files with folders or vice versa. The library works across different filesystems.</p>"

description2:
  enable: 1
  title: Project Result
  text1: "<p><b>mergepath</b> simplifies the process of merging directories in GoLang projects. It offers functionality to handle file links and intelligently resolve conflicts when overwriting files with folders or vice versa. The library is designed to work seamlessly across different filesystems. The project has gained recognition for its usefulness in file management and directory merging tasks.</p>"

gallery:
  enable: 0
  images:
    - images/works/work5.jpeg
    - images/blog/blog8.jpg
    - images/blog/blog6.jpg

video:
  enable: 0
  poster: images/blog/blog9.jpg
  id: Gu6z6kIukgg

---
