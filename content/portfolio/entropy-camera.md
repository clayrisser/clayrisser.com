---
# preview details
title: Entropy Camera
category: Linux
category_slug: linux
image: images/portfolio/entropy-camera.png
short_description: A tool for feeding entropy data to Linux from a camera.

# full details
live_preview: https://github.com/clayrisser/entropy-camera
full_image: images/portfolio/entropy-camera.png
info:
  - label: Year
    value: 2019-Present
  - label: Technology
    value: Node.js

description1:
  enable: 1
  title: Project Goal
  text1: "<p>The <b>entropy-camera</b> project focuses on providing a means to feed entropy data to Linux systems from a camera. It offers a tool that utilizes the camera as a source of entropy, which can be beneficial for improving the randomness of cryptographic operations on Linux systems.</p>"

description2:
  enable: 1
  title: Project Result
  text1: "<p><b>entropy-camera</b> allows users to utilize camera data as a source of entropy on Linux systems. By leveraging the camera to feed entropy, it contributes to enhancing the randomness required for cryptographic operations. The project has been recognized for its practicality and potential impact on improving security.</p>"

gallery:
  enable: 0
  images:
    - images/works/work5.jpeg
    - images/blog/blog8.jpg
    - images/blog/blog6.jpg

video:
  enable: 0
  poster: images/blog/blog9.jpg
  id: Gu6z6kIukgg

---
