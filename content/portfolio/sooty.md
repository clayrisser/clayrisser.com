---
# preview details
title: Sooty
category: DevOps
category_slug: devops
image: images/portfolio/sooty.png
short_description: A web scraping tool built on headless Chrome.

# full details
live_preview: https://github.com/clayrisser/sooty
full_image: images/portfolio/sooty.png
info:
  - label: Year
    value: 2018-Present
  - label: Technology
    value: NodeJS, Chrome

description1:
  enable: 1
  title: Project Goal
  text1: "<p><b>Sooty</b> is a web scraping tool built on headless Chrome. It allows users to scrape websites using query selectors and interact with web pages through automated interactions. By creating Sooty, I aimed to provide a flexible and powerful web scraping solution that leverages the capabilities of headless Chrome.</p>
  <p>This project showcases my expertise in NodeJS and Chrome automation, demonstrating my ability to extract data from websites and automate repetitive tasks efficiently.</p>"

gallery:
  enable: 0
  images:
    - images/works/work5.jpeg
    - images/blog/blog8.jpg
    - images/blog/blog6.jpg

description2:
  enable: 1
  title: Project Result
  text1: "<p>The <b>Sooty</b> project has been a useful tool for web scraping tasks, providing a simple yet powerful interface to scrape websites and extract desired information. It is built on the foundation of headless Chrome, offering users the ability to interact with web pages and perform automated actions.</p>
  <p>This project contributes to my portfolio of DevOps-related projects, highlighting my ability to leverage NodeJS and Chrome to build practical tools for data extraction and web automation.</p>"

video:
  enable: 0
  poster: images/blog/blog9.jpg
  id: Gu6z6kIukgg

---
