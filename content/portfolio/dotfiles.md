---
# preview details
title: Dotfiles
category: Configuration
category_slug: configuration 
image: images/portfolio/dotfiles.png
short_description: My dotfiles - enjoy!

# full details
live_preview: https://github.com/clayrisser/dotfiles
full_image: images/portfolio/dotfiles.png
info:
  - label: Year
    value: 2017-Present
  - label: Technology
    value: GNU Make, Stow

description1:
  enable: 1
  title: Project Goal
  text1: "<p>The <b>dotfiles</b> project consists of my personal dotfiles, which are configuration files and scripts used to customize my development environment. This project allows me to maintain a consistent and efficient setup across multiple systems and platforms.</p>
  <p>By sharing my dotfiles, I aim to provide inspiration and ideas to others looking to enhance their own development environments. It also serves as a reflection of my attention to detail and commitment to productivity.</p>"

gallery:
  enable: 0
  images:
    - images/works/work5.jpeg
    - images/blog/blog8.jpg
    - images/blog/blog6.jpg

description2:
  enable: 1
  title: Project Result
  text1: "<p>The <b>dotfiles</b> project has been well-received by the developer community, and it continues to evolve as I refine and update my personal configuration. By maintaining my dotfiles publicly, I demonstrate my dedication to sharing knowledge and contributing to the open-source community.</p>
  <p>This project showcases my expertise in customizing development environments and provides insights into my preferred tools and workflows.</p>"

video:
  enable: 0
  poster: images/blog/blog9.jpg
  id: Gu6z6kIukgg


---
