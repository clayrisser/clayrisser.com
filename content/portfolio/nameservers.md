---
# preview details
title: Nameservers
category: Configuration
category_slug: configuration
image: images/portfolio/nameservers.png
short_description: A tool for listing your nameservers.

# full details
live_preview: https://github.com/clayrisser/nameservers
full_image: images/portfolio/nameservers.png
info:
  - label: Year
    value: 2017-Present
  - label: Technology
    value: Node.js

description1:
  enable: 1
  title: Project Goal
  text1: "<p>The <b>nameservers</b> project aims to provide a tool for listing the nameservers configured on a system. It allows users to easily retrieve and view the nameserver addresses being used by their system.</p>"

description2:
  enable: 1
  title: Project Result
  text1: "<p><b>nameservers</b> offers a straightforward way to list the configured nameservers. By running the tool, users can quickly view the nameserver addresses used by their system. The project has gained popularity for its simplicity and usefulness in troubleshooting network and DNS-related issues.</p>"

gallery:
  enable: 0
  images:
    - images/works/work5.jpeg
    - images/blog/blog8.jpg
    - images/blog/blog6.jpg

video:
  enable: 0
  poster: images/blog/blog9.jpg
  id: Gu6z6kIukgg
---
