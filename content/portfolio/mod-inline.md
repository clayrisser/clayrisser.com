---
# preview details

title: Mod Inline
category: Development
category_slug: development
image: images/portfolio/mod-inline.png
short_description: Easily modify strings inline with regular expressions.

# full details

live_preview: https://github.com/clayrisser/mod-inline
full_image: images/portfolio/mod-inline.png
info:

- label: Year
  value: 2018-Present
- label: Technology
  value: Node.js

description1:
  enable: 1
  title: Project Goal
  text1: "<p>The <b>Mod Inline</b> project provides a simple way to modify strings inline using regular expressions. It allows developers to isolate sections, apply regular expressions, and append text within strings. This tool enhances productivity by simplifying string modifications, making it ideal for various Node.js applications.</p>

  <p>This project showcases my proficiency in working with Node.js and my ability to develop practical tools that streamline development processes.</p>"

gallery:
  enable: 0
  images: 
    - images/works/work1.jpeg
    - images/works/work2.jpeg
    - images/works/work3.jpeg 

description2:
  enable: 1
  title: Project Result
  text1: "<p>The <b>Mod Inline</b> project has received positive feedback from the developer community. It has proven to be a valuable solution for efficiently modifying strings with regular expressions. By actively maintaining and enhancing this project, I continue to demonstrate my expertise in Node.js development and my dedication to supporting fellow developers.</p>"

video:
  enable: 0
  poster: images/blog/blog9.jpg
  id: Gu6z6kIukgg

---
