---
# preview details
title: Generator Reaction
category: Development
category_slug: development
image: images/portfolio/generator-reaction.png
short_description: Yeoman generator for the Reaction framework.

# full details
live_preview: https://github.com/clayrisser/generator-reaction
full_image: images/portfolio/generator-reaction.png
info:
  - label: Year
    value: 2018-Present
  - label: Technology
    value: Node.js, Yeoman, Reaction framework

description1:
  enable: 1
  title: Project Goal
  text1: "<p>The <b>generator-reaction</b> project aimed to provide a Yeoman generator for the Reaction framework. It simplifies the setup and scaffolding process for creating apps with the Reaction framework, which supports Native iOS, Native Android, Web, and Expo platforms. The generator helps developers bootstrap their projects quickly and efficiently.</p>"

description2:
  enable: 1
  title: Project Result
  text1: "<p><b>generator-reaction</b> is a powerful tool that streamlines the development workflow for creating apps with the Reaction framework. It offers support for multiple platforms and provides an easy-to-use interface for generating project structures. The project has been well-received by developers and has contributed to the adoption and growth of the Reaction framework.</p>"

gallery:
  enable: 0
  images:
    - images/works/work5.jpeg
    - images/blog/blog8.jpg
    - images/blog/blog6.jpg

video:
  enable: 0
  poster: images/blog/blog9.jpg
  id: Gu6z6kIukgg
---
