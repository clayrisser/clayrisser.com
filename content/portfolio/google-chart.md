---
#preview details
title: Google Chart
category: DevOps
category_slug: devops
image: images/portfolio/google-chart.png
short_description: An unopinionated way to implement Google Charts into your React projects.

# full details
live_preview: https://jamrizzi.github.io/google-chart-react
full_image: images/portfolio/google-chart.png
info:
  - label: Year
    value: 2016
  - label: Technology
    value: React, Google Charts

description1:
  enable: 1
  title: Project Goal
  text1: "<p>The <b>Google Chart</b> project was created to offer developers an unopinionated approach to integrate Google Charts into their React projects. It provides an implementation that closely aligns with the guidelines presented in the official Google Chart documentation. </p>"

gallery:
  enable: 0
  images:
    - images/works/work5.jpeg
    - images/blog/blog8.jpg
    - images/blog/blog6.jpg

description2:
  enable: 1
  title: Project Result
  text1: "<p><This project showcases my proficiency in React development and my ability to incorporate powerful visualization tools into modern front-end frameworks, thereby enabling developers to leverage the full potential of Google Charts in their React applications.</p>"

video:
enable: 0
poster: images/blog/blog9.jpg
id: Gu6z6kIukgg
---
