---
# preview details
title: Cors Server
category: DevOps
category_slug: devops
image: images/portfolio/cors-server.png
short_description: A Dockerfile to test CORS (Cross-Origin Resource Sharing).

# full details
live_preview: https://github.com/clayrisser/cors-server
full_image: images/portfolio/cors-server.png
info:
  - label: Year
    value: 2018-Present
  - label: Technology
    value: Node.js

description1:
  enable: 1
  title: Project Goal
  text1: "<p>The <b>cors-server</b> project aims to provide a Dockerfile to test Cross-Origin Resource Sharing (CORS). It allows developers to quickly spin up a server with CORS support to test CORS functionality and OPTIONS preflight requests.</p>"

description2:
  enable: 1
  title: Project Result
  text1: "<p><b>cors-server</b> simplifies the process of testing CORS functionality by providing a ready-to-use Dockerfile. Developers can easily launch a server with CORS support and perform CORS-related tests, including OPTIONS preflight requests. The project has gained recognition for its usefulness in CORS development and troubleshooting.</p>"

gallery:
  enable: 0
  images:
    - images/works/work5.jpeg
    - images/blog/blog8.jpg
    - images/blog/blog6.jpg

video:
  enable: 0
  poster: images/blog/blog9.jpg
  id: Gu6z6kIukgg

---
