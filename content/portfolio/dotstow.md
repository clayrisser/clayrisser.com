---
# preview details
title: Dotstow
category: Development
category_slug: development
image: images/portfolio/dotstow.png
short_description: Manage dotfiles with Git and Stow.

# full details
live_preview: https://github.com/clayrisser/dotstow
full_image: images/portfolio/dotstow.png
info:
  - label: Year
    value: 2017-Present
  - label: Technology
    value: Git, Stow

description1:
  enable: 1
  title: Project Goal
  text1: "<p>The <b>dotstow</b> project aims to provide a streamlined solution for managing dotfiles using Git and Stow. It allows users to keep their dotfiles in a version control system and easily synchronize them across multiple computers, regardless of the operating system.</p>
  <p>By developing this tool, I demonstrate my expertise in Git and showcase my ability to create practical solutions for managing dotfiles in a cross-platform manner.</p>"

gallery:
  enable: 0
  images:
    - images/works/work5.jpeg
    - images/blog/blog8.jpg
    - images/blog/blog6.jpg

description2:
  enable: 1
  title: Project Result
  text1: "<p>The <b>dotstow</b> project has proven to be an effective tool for managing dotfiles with ease and consistency. It simplifies the process of syncing dotfiles across different machines, including those with diverse operating systems.</p>
  <p>This project contributes to my portfolio of DevOps-related projects, showcasing my ability to leverage Git and Stow to create efficient and flexible dotfile management solutions.</p>"

video:
  enable: 0
  poster: images/blog/blog9.jpg
  id: Gu6z6kIukgg
---
