---
# preview details
title: Charts
category: DevOps
category_slug: devops
image: images/portfolio/charts.png
short_description: An extensive collection of helm charts for Kubernetes, demonstrating deep knowledge and experience.

# full details
live_preview: https://github.com/clayrisser/charts
full_image: images/portfolio/charts.png
info:
  - label: Year
    value: 2018-Present
  - label: Technology
    value: Kubernetes, Helm

description1:
  enable: 1
  title: Project Goal
  text1: "<p>The <b>Charts</b> project was an endeavour to demonstrate proficiency and comprehensive knowledge of Kubernetes. The primary goal was to create an array of helm charts addressing various use-cases and deployment scenarios within the Kubernetes environment.</p>
  <p>The charts were developed keeping versatility, reusability, and maintainability in mind, catering to a broad spectrum of Kubernetes applications.</p>"

gallery:
  enable: 0
  images:
    - images/works/work5.jpeg
    - images/blog/blog8.jpg
    - images/blog/blog6.jpg

description2:
  enable: 1
  title: Project Result
  text1: "<p>With over 4,000 commits mostly made by myself, the <b>Charts</b> project is a testament to my hands-on experience with Kubernetes. The project has evolved to include dozens of helm charts and has been actively contributed to since 2018.</p>
  <p>The charts have been designed to accommodate a range of Kubernetes applications, further strengthening the project's utility in the DevOps community.</p>"

video:
  enable: 0
  poster: images/blog/blog9.jpg
  id: Gu6z6kIukgg
---
