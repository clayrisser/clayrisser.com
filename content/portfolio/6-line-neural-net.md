---
# preview details
title: 6 Line Neural Net
category: Machine Learning
category_slug: machine-learning
image: images/portfolio/6-line-neural-net.png
short_description: The smallest neural network, loosely based on iamtrask's 11 line neural net.

# full details
live_preview: https://github.com/clayrisser/6-line-neural-net
full_image: images/portfolio/6-line-neural-net.png
info:
  - label: Year
    value: 2017-Present
  - label: Technology
    value: Python, numpy

description1:
  enable: 1
  title: Project Goal
  text1: "<p>The <b>6-line-neural-net</b> project was initiated with the aim of creating the smallest possible neural network. It was loosely based on iamtrask's 11 line neural net.</p>"

description2:
  enable: 1
  title: Project Result
  text1: "<p>The <b>6-line-neural-net</b> is a powerful testament to minimalistic coding and efficient use of the numpy library. Despite its small size, it effectively demonstrates the functionality of a neural network.</p>"

gallery:
  enable: 0
  images:
    - images/works/work6.jpeg
    - images/blog/blog8.jpg
    - images/blog/blog6.jpg

video:
  enable: 0
  poster: images/blog/blog9.jpg
  id: Gu6z6kIukgg
---
