---
# preview details
title: TypeGraphQL Prisma Redux
category: React
category_slug: react
image: images/portfolio/typegraphql-prisma-redux.png
short_description: Generate Redux from Prisma schemas to query TypeGraphQL.

# full details
live_preview: https://github.com/clayrisser/typegraphql-prisma-redux
full_image: images/portfolio/typegraphql-prisma-redux.png
info:
  - label: Year
    value: 2019-Present
  - label: Technology
    value: Node.js

description1:
  enable: 1
  title: Project Goal
  text1: "<p>The <b>TypeGraphQL Prisma Redux</b> project aims to simplify the process of generating Redux code from Prisma schemas to query TypeGraphQL. By automating this process, it provides developers with an efficient way to integrate Redux into their TypeGraphQL-based applications, enhancing the state management capabilities.</p>"

gallery:
  enable: 0
  images:
    - images/works/work5.jpeg
    - images/blog/blog8.jpg
    - images/blog/blog6.jpg

description2:
  enable: 1
  title: Project Result
  text1: "<p>The <b>TypeGraphQL Prisma Redux</b> project has been well-received by developers working with Prisma and TypeGraphQL. By simplifying the integration of Redux and providing an automated code generation approach, it streamlines the development process and improves state management in applications.</p>"

video:
  enable: 0
  poster: images/blog/blog9.jpg
  id: Gu6z6kIukgg
---
