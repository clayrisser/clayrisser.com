---
# preview details
title: Kustomize Operator
category: DevOps
category_slug: devops
image: images/portfolio/kustomize-operator.png
short_description: A Kubernetes operator built to leverage Kustomize through Helm.

# full details
live_preview: https://github.com/clayrisser/kustomize-operator
full_image: images/portfolio/kustomize-operator.png
info:
  - label: Year
    value: 2019-Present
  - label: Technology
    value: Kubernetes, Operator SDK, Helm, Kustomize

description1:
  enable: 1
  title: Project Goal
  text1: "<p>The <b>Kustomize Operator</b> project was created to enable the efficient and effective use of Kustomize in conjunction with Helm. The project was designed to bypass the need for forking and maintaining 3rd party Helm charts, instead providing a way to patch these charts directly.</p>
  <p>This operator shows my deep understanding of Kubernetes and its ecosystem, showcasing my ability to build practical solutions to complex issues.</p>"

gallery:
  enable: 0
  images:
    - images/works/work5.jpeg
    - images/blog/blog8.jpg
    - images/blog/blog6.jpg

description2:
  enable: 1
  title: Project Result
  text1: "<p>The <b>Kustomize Operator</b> project has been an effective solution for managing Helm charts without unnecessary forking or manual maintenance. This operator underscores my capabilities to utilize different Kubernetes technologies in tandem for the creation of efficient DevOps tools.</p>
  <p>The project continues to contribute to my comprehensive portfolio of Kubernetes related projects, demonstrating my deep knowledge and experience in the field.</p>"

video:
  enable: 0
  poster: images/blog/blog9.jpg
  id: Gu6z6kIukgg
---
