---
# preview details
title: Export ES6
category: Node.js
category_slug: nodejs
image: images/portfolio/export-es6.png
short_description: An utility for exporting ES6 modules in a CommonJS environment.

# full details
live_preview: https://github.com/clayrisser/export-es6
full_image: images/portfolio/export-es6.png
info:
  - label: Year
    value: 2018-Present
  - label: Technology
    value: Node.js

description1:
  enable: 1
  title: Project Goal
  text1: "<p>The <b>export-es6</b> project aims to provide a utility for exporting ES6 modules in a CommonJS environment. It allows developers to export both CommonJS and ES6 modules seamlessly, ensuring compatibility across different module systems.</p>"

description2:
  enable: 1
  title: Project Result
  text1: "<p><b>export-es6</b> enables developers to export modules in a CommonJS environment while providing compatibility with ES6 modules. It simplifies the process of exporting modules, making it easier to write code that can be consumed by different module systems. The project has received positive feedback for its usefulness in ensuring smooth interoperability between module systems.</p>"

gallery:
  enable: 0
  images:
    - images/works/work5.jpeg
    - images/blog/blog8.jpg
    - images/blog/blog6.jpg

video:
  enable: 0
  poster: images/blog/blog9.jpg
  id: Gu6z6kIukgg

---
