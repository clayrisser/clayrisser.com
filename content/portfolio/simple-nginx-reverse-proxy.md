---
# preview details
title: Simple Nginx Reverse Proxy
category: DevOps
category_slug: docker
image: images/portfolio/simple-nginx-reverse-proxy.png
short_description: A simple nginx reverse proxy for Docker, allowing easy configuration with environment variables.

# full details
live_preview: https://github.com/clayrisser/simple-nginx-reverse-proxy
full_image: images/portfolio/simple-nginx-reverse-proxy.png
info:
  - label: Year
    value: 2018-Present
  - label: Technology
    value: Docker

description1:
  enable: 1
  title: Project Goal
  text1: "<p>The <b>Simple Nginx Reverse Proxy</b> project aims to provide a straightforward solution for setting up an nginx reverse proxy in Docker environments. It allows easy configuration through environment variables, making it convenient to redirect incoming traffic to different services.</p>"

description2:
  enable: 1
  title: Project Result
  text1: "<p><b>Simple Nginx Reverse Proxy</b> simplifies the process of setting up a reverse proxy with nginx in Docker-compose environments. By leveraging Docker's flexibility, it enables users to easily configure and manage reverse proxy settings using environment variables. The project has been well-received for its simplicity and contribution to Docker-based deployments.</p>"

gallery:
  enable: 0
  images:
    - images/works/work5.jpeg
    - images/blog/blog8.jpg
    - images/blog/blog6.jpg

video:
  enable: 0
  poster: images/blog/blog9.jpg
  id: Gu6z6kIukgg

---
