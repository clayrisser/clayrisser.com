---
# Preview Details
title: Ory Stack
category: DevOps
category_slug: devops
image: images/portfolio/ory-stack.png
short_description: Ory Hydra, Oathkeeper, and Keto.

# Full Details
live_preview: https://github.com/clayrisser/ory-stack
full_image: images/portfolio/ory-stack.png
info:
  - label: Year
    value: 2018-Present
  - label: Technology
    value: Docker

description1:
  enable: 1
  title: Project Goal
  text1: "<p>The <b>Ory Stack</b> project brings together Ory Hydra, Oathkeeper, and Keto to provide a comprehensive solution for authentication, authorization, and access control in cloud-native applications. It leverages Docker and provides a pre-configured environment for quickly setting up and deploying these powerful tools.</p>
  <p>This project showcases my expertise in containerization using Docker and my ability to integrate multiple DevOps tools to create robust solutions for secure application development.</p>"

gallery:
  enable: 0
  images:
    - images/works/work5.jpeg
    - images/blog/blog8.jpg
    - images/blog/blog6.jpg

description2:
  enable: 1
  title: Project Result
  text1: "<p>The <b>Ory Stack</b> project has gained significant popularity among developers and DevOps professionals seeking a secure and scalable solution for authentication and authorization. It simplifies the implementation of these critical components and allows developers to focus on building their applications.</p>
  <p>By actively maintaining and contributing to the Ory Stack project, I demonstrate my commitment to improving security practices in cloud-native environments and supporting the developer community.</p>"

video:
  enable: 0
  poster: images/blog/blog9.jpg
  id: Gu6z6kIukgg

---
