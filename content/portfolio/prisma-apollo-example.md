---
# Preview Details
title: Prisma Apollo Example
category: DevOps
category_slug: devops
image: images/portfolio/prisma-apollo-example.png
short_description: An example of using Prisma as an Apollo datasource.

# Full Details
live_preview: https://github.com/clayrisser/prisma-apollo-example
full_image: images/portfolio/prisma-apollo-example.png
info:
  - label: Year
    value: 2019-Present
  - label: Technology
    value: Prisma, Apollo, Photon

description1:
  enable: 1
  title: Project Goal
  text1: "<p>The <b>Prisma Apollo Example</b> project was created to demonstrate the integration of Prisma as an Apollo datasource. The project aims to provide a practical example of how these technologies can be combined to develop efficient and scalable applications.</p>
  <p>This project exhibits my proficiency in working with modern technologies like Prisma, Apollo, and Photon, and my dedication to contributing valuable resources to the developer community.</p>"

gallery:
  enable: 0
  images:
    - images/works/work5.jpeg
    - images/blog/blog8.jpg
    - images/blog/blog6.jpg

description2:
  enable: 1
  title: Project Result
  text1: "<p>The <b>Prisma Apollo Example</b> project has been well-received by developers looking to understand the integration of Prisma with Apollo. It provides a hands-on, practical example that demonstrates how these technologies can be effectively combined to build scalable applications.</p>
  <p>By actively maintaining and expanding this project, I continue to showcase my expertise in modern development technologies and my commitment to creating resources that help developers improve their skills.</p>"

video:
  enable: 0
  poster: images/blog/blog9.jpg
  id: Gu6z6kIukgg

---
