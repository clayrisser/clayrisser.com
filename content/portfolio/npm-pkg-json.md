---
# preview details
title: Npm Pkg Json
category: Development
category_slug: development
image: images/portfolio/npm-pkg-json.png
short_description: A package to load package.json files in Node.js projects.

# full details
live_preview: https://github.com/clayrisser/npm-pkg-json
full_image: images/portfolio/npm-pkg-json.png
info:
  - label: Year
    value: 2019-Present
  - label: Technology
    value: Node.js

description1:
  enable: 1
  title: Project Goal
  text1: "<p>The <b>npm-pkg-json</b> project aims to provide a simple solution for loading package.json files in Node.js projects, even when the rootDir is out of scope. It allows developers to easily access and consume package.json data, supporting both JavaScript and TypeScript projects.</p>"

description2:
  enable: 1
  title: Project Result
  text1: "<p><b>npm-pkg-json</b> simplifies the process of accessing and utilizing package.json data in Node.js projects. It provides a seamless way to load package.json files, even in scenarios where the rootDir is not within the current scope. The project has gained recognition for its convenience and contribution to improving package.json handling practices.</p>"

gallery:
  enable: 0
  images:
    - images/works/work5.jpeg
    - images/blog/blog8.jpg
    - images/blog/blog6.jpg

video:
  enable: 0
  poster: images/blog/blog9.jpg
  id: Gu6z6kIukgg
---
