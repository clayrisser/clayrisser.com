---
# preview details
title: Reaction Build
category: Development
category_slug: development
image: images/portfolio/reaction-build.png
short_description: A build tool for Reaction, a React-based e-commerce platform.

# full details
live_preview: https://github.com/clayrisser/reaction-build
full_image: images/portfolio/reaction-build.png
info:
  - label: Year
    value: 2018-Present
  - label: Technology
    value: Node.js

description1:
  enable: 1
  title: Project Goal
  text1: "<p>The <b>Reaction Build</b> project aims to provide a build tool specifically designed for Reaction, a React-based e-commerce platform. It facilitates the efficient building and compilation of assets for Reaction projects, ensuring optimal performance and customization options.</p>"

description2:
  enable: 1
  title: Project Result
  text1: "<p><b>Reaction Build</b> has become an essential component for developers working with Reaction. It streamlines the build process, allowing for seamless asset compilation and optimization. The project has gained recognition for its contribution to the Reaction ecosystem and its impact on improving development workflows.</p>"

gallery:
  enable: 0
  images:
    - images/works/work5.jpeg
    - images/blog/blog8.jpg
    - images/blog/blog6.jpg

video:
  enable: 0
  poster: images/blog/blog9.jpg
  id: Gu6z6kIukgg

---
