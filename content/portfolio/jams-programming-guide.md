---
# Preview Details
title: Jam's Programming Guide
category: Development
category_slug: development
image: images/portfolio/jams-programming-guide.png
short_description: An opinionated programming guide for consistent and collaborative programming.

# Full Details
live_preview: https://github.com/clayrisser/jams-programming-guide
full_image: images/portfolio/jams-programming-guide.png
info:
  - label: Year
    value: 2017-Present
  - label: Technology
    value: Programming Guidelines

description1:
  enable: 1
  title: Project Goal
  text1: "<p>The <b>Jam's Programming Guide</b> project was created to establish an opinionated set of programming guidelines for consistent and collaborative programming. It provides a comprehensive guide covering various programming constructs and best practices, aiming to improve code quality and maintainability.</p>
  <p>This project demonstrates my dedication to promoting consistent coding standards and my commitment to enhancing collaboration among developers.</p>"

gallery:
  enable: 0
  images:
    - images/works/work5.jpeg
    - images/blog/blog8.jpg
    - images/blog/blog6.jpg

description2:
  enable: 1
  title: Project Result
  text1: "<p>The <b>Jam's Programming Guide</b> project has been well-received by the programming community and has proven to be a valuable resource for developers seeking guidance on writing clean and maintainable code. It reflects my passion for promoting best practices and fostering a collaborative coding culture.</p>
  <p>By actively maintaining and expanding this project, I continue to showcase my expertise in programming and my dedication to supporting developers in their coding journey.</p>"

video:
  enable: 0
  poster: images/blog/blog9.jpg
  id: Gu6z6kIukgg


---
