---
# preview details
title: Mkpm
category: Configuration
category_slug: configuration
image: images/portfolio/mkpm.png
short_description: A package manager for Makefile projects, providing easy integration of packages and utilities.

# full details
live_preview: https://github.com/clayrisser/mkpm
full_image: images/portfolio/mkpm.png
info:
  - label: Year
    value: 2021-Present
  - label: Technology
    value: GNU Make, Git

description1:
  enable: 1
  title: Project Goal
  text1: "<p>The <b>mkpm</b> project aims to simplify package management in Makefile projects. It provides a package manager that enables easy integration of packages and utilities within a Makefile-based project structure.</p>"

description2:
  enable: 1
  title: Project Result
  text1: "<p><b>mkpm</b> streamlines the process of managing packages and dependencies in Makefile projects, improving project organization and reusability. It has become a valuable tool for developers working with Makefile-based projects, offering a convenient way to incorporate external packages and utilities.</p>"

gallery:
  enable: 0
  images:
    - images/works/work5.jpeg
    - images/blog/blog8.jpg
    - images/blog/blog6.jpg

video:
  enable: 0
  poster: images/blog/blog9.jpg
  id: Gu6z6kIukgg
---
