---
# preview details
title: React AST
category: React
category_slug: react
image: images/portfolio/react-ast.png
short_description: An innovative project that renders abstract syntax trees using React, enabling composable code generators.

# full details
live_preview: https://github.com/clayrisser/react-ast
full_image: images/portfolio/react-ast.png
info:
  - label: Year
    value: 2020-Present
  - label: Technology
    value: React, Abstract Syntax Trees

description1:
  enable: 1
  title: Project Goal
  text1: "<p>The <b>React AST</b> project aimed to use the React framework for a new purpose: rendering abstract syntax trees (ASTs). The project seeks to make code generation more intuitive and flexible, employing the familiar React component model.</p>"

description2:
  enable: 1
  title: Project Result
  text1: "<p><b>React AST</b> is an innovative project that combines familiar React paradigms with abstract syntax tree rendering. This approach allows for dynamic generation of codebases, such as the React GTK project. It has become a community favorite for its unique use of React and its contribution to code generation methods.</p>"

gallery:
  enable: 0
  images:
    - images/works/work5.jpeg
    - images/blog/blog8.jpg
    - images/blog/blog6.jpg

video:
  enable: 0
  poster: images/blog/blog9.jpg
  id: Gu6z6kIukgg
---
