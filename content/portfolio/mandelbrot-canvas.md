---
# preview details
title: Mandelbrot Canvas
category: Development
category_slug: development
image: images/portfolio/mandelbrot-canvas.png
short_description: A Mandelbrot set renderer using canvas.

# full details
live_preview: https://github.com/clayrisser/mandelbrot-canvas
full_image: images/portfolio/mandelbrot-canvas.png
info:
  - label: Year
    value: 2018-Present
  - label: Technology
    value: Node.js

description1:
  enable: 1
  title: Project Goal
  text1: "<p>The <b>mandelbrot-canvas</b> project aims to provide a Mandelbrot set renderer using canvas. It allows users to generate and visualize the intricate patterns of the Mandelbrot set by rendering it to a canvas element.</p>"

description2:
  enable: 1
  title: Project Result
  text1: "<p><b>mandelbrot-canvas</b> provides an easy way to render and explore the Mandelbrot set using canvas. It includes features such as zooming and panning, allowing users to navigate and focus on specific areas of the fractal. The project has gained popularity for its ability to create stunning visualizations of the Mandelbrot set.</p>"

gallery:
  enable: 0
  images:
    - images/works/work5.jpeg
    - images/blog/blog8.jpg
    - images/blog/blog6.jpg

video:
  enable: 0
  poster: images/blog/blog9.jpg
  id: Gu6z6kIukgg

---
