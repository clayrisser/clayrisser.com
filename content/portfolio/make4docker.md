---
# Preview Details
title: Make4Docker
category: DevOps
category_slug: devops
image: images/portfolio/make4docker.png
short_description: Utilize GNU Make to simplify your Docker builds.

# Full Details
live_preview: https://github.com/clayrisser/make4docker
full_image: images/portfolio/make4docker.png
info:
  - label: Year
    value: 2017-Present
  - label: Technology
    value: GNU Make, Docker

description1:
  enable: 1
  title: Project Goal
  text1: "<p>The <b>Make4Docker</b> project was developed with the objective of using GNU Make to simplify Docker build processes. It provides a set of make commands to streamline the process of pulling, building, running, and pushing Docker images, and even allows for SSHing into images or running containers.</p>
  <p>This project demonstrates my ability to leverage the power of GNU Make in improving the Docker workflow, showcasing my aptitude for developing practical and efficient DevOps tools.</p>"

gallery:
  enable: 0
  images:
    - images/works/work5.jpeg
    - images/blog/blog8.jpg
    - images/blog/blog6.jpg

description2:
  enable: 1
  title: Project Result
  text1: "<p>The <b>Make4Docker</b> project has been positively received by the Docker community, providing a straightforward and streamlined approach to managing Docker builds. The tool embodies my commitment to creating efficient tools that improve the workflow for developers and DevOps professionals alike.</p>
  <p>Through active maintenance and improvements of this project, I continue to highlight my proficiency in DevOps practices and my dedication to supporting the developer community.</p>"

video:
  enable: 0
  poster: images/blog/blog9.jpg
  id: Gu6z6kIukgg

---
