---
# Preview Details
title: Prerender
category: DevOps
category_slug: devops
image: images/portfolio/prerender.png
short_description: Prerender BlogDown using event listeners and timeouts.

# Full Details
live_preview: https://github.com/clayrisser/prerender
full_image: images/portfolio/prerender.png
info:
  - label: Year
    value: 2017-Present
  - label: Technology
    value: Docker

description1:
  enable: 1
  title: Project Goal
  text1: "<p>The <b>Prerender BlogDown</b> project was created to enable prerendering of BlogDown using event listeners and timeouts. It aims to improve the performance of BlogDown by prerendering it before serving, reducing the load time for users. By using Docker and incorporating event listeners and timeouts, this project provides an efficient solution for prerendering BlogDown websites.</p>
  <p>This project demonstrates my expertise in Docker and showcases my ability to optimize web applications through innovative prerendering techniques.</p>"

gallery:
  enable: 0
  images:
    - images/works/work5.jpeg
    - images/blog/blog8.jpg
    - images/blog/blog6.jpg

description2:
  enable: 1
  title: Project Result
  text1: "<p>The <b>Prerender BlogDown</b> project has gained positive feedback from the developer community for its ability to significantly improve the performance of BlogDown websites. By prerendering the content, users experience faster load times and improved user experience. This project highlights my dedication to optimizing web applications and creating innovative solutions that improve overall user experience.</p>
  <p>By actively maintaining and updating this project, I continue to showcase my expertise in Docker and my dedication to developing solutions that enhance web application performance.</p>"

video:
  enable: 0
  poster: images/blog/blog9.jpg
  id: Gu6z6kIukgg

---
