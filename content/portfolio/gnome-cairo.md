---
# preview details
title: Gnome Cairo
category: Configuration
category_slug: configuration
image: images/portfolio/gnome-cairo.png
short_description: TypeScript bindings for GNOME Cairo.

# full details
live_preview: https://github.com/clayrisser/gnome-cairo
full_image: images/portfolio/gnome-cairo.png
info:
  - label: Year
    value: 2019-Present
  - label: Technology
    value: Node.js

description1:
  enable: 1
  title: Project Goal
  text1: "<p>The <b>gnome-cairo</b> project aims to provide TypeScript bindings for GNOME Cairo, enabling developers to access the full Cairo API from TypeScript. It allows developers to leverage the powerful capabilities of the Cairo graphics library in their TypeScript projects.</p>"

description2:
  enable: 1
  title: Project Result
  text1: "<p><b>gnome-cairo</b> offers comprehensive TypeScript bindings for GNOME Cairo. By utilizing these bindings, developers can access the entire Cairo API from TypeScript, enhancing their ability to create and manipulate graphics in their projects. The project has been well-received for its contribution to enabling Cairo-based development with TypeScript.</p>"

gallery:
  enable: 0
  images:
    - images/works/work5.jpeg
    - images/blog/blog8.jpg
    - images/blog/blog6.jpg

video:
  enable: 0
  poster: images/blog/blog9.jpg
  id: Gu6z6kIukgg

---
