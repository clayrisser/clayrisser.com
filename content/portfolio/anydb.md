---
# preview details
title: Any DB
category: Database
category_slug: database
image: images/portfolio/anydb.png
short_description: Run any database.

# full details
live_preview: https://github.com/clayrisser/anydb
full_image: images/portfolio/anydb.png
info:
  - label: Year
    value: 2018-Present
  - label: Technology
    value: Python 3, Docker

description1:
  enable: 1
  title: Project Goal
  text1: "<p>The <b>any-database</b> project aims to provide a flexible solution for running various databases. It supports popular databases such as MySQL, MariaDB, Postgres, and MongoDB, allowing users to easily spin up and manage different database instances.</p>
  <p>By developing this tool, I demonstrate my expertise in Python 3 and Docker, showcasing my ability to create practical solutions for working with databases across different projects and environments.</p>"

gallery:
  enable: 0
  images:
    - images/works/work5.jpeg
    - images/blog/blog8.jpg
    - images/blog/blog6.jpg

description2:
  enable: 1
  title: Project Result
  text1: "<p>The <b>any-database</b> project has proven to be a versatile solution for running databases without the need for manual setup or configuration. It provides support for multiple database technologies, enabling developers to seamlessly switch between different databases based on project requirements.</p>
  <p>This project contributes to my portfolio of DevOps-related projects, highlighting my ability to leverage Python and Docker to create efficient tools for managing databases across various applications.</p>"

video:
  enable: 0
  poster: images/blog/blog9.jpg
  id: Gu6z6kIukgg

---
