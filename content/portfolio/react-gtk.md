---
# preview details
title: React GTK
category: React
category_slug: react
image: images/portfolio/react-gtk.png
short_description: A React renderer built to interface with the GTK toolkit for cross-platform native desktop applications.

# full details
live_preview: https://github.com/clayrisser/react-gtk
full_image: images/portfolio/react-gtk.png
info:
  - label: Year
    value: 2019-Present
  - label: Technology
    value: React, GTK

description1:
  enable: 1
  title: Project Goal
  text1: "<p>The <b>React GTK</b> project was initiated to build a React renderer interfacing with the GTK toolkit. The idea was to create a library for building cross-platform native desktop applications leveraging the familiar and powerful React paradigm.</p>"

description2:
  enable: 1
  title: Project Result
  text1: "<p>With over 250 stars on GitHub, <b>React GTK</b> has become one of the community's favorite projects. It showcases how innovative thinking and a deep understanding of both React and GTK can result in an impressive and functional tool for UI development.</p>"

gallery:
  enable: 0
  images:
    - images/works/work5.jpeg
    - images/blog/blog8.jpg
    - images/blog/blog6.jpg

video:
  enable: 0
  poster: images/blog/blog9.jpg
  id: Gu6z6kIukgg
---
