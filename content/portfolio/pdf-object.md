---
# preview details
title: PDF Object
category: Development
category_slug: development
image: images/portfolio/pdf-object.png
short_description: An embeddable PDF viewer for Polymer.

# full details
live_preview: https://github.com/clayrisser/pdf-object
full_image: images/portfolio/pdf-object.png
info:
  - label: Year
    value: 2016-Present
  - label: Technology
    value: Polymer

description1:
  enable: 1
  title: Project Goal
  text1: "<p>The <b>PDF Object</b> project aims to provide an embeddable PDF viewer for Polymer, a web component library. It enables developers to easily display and interact with PDF files within Polymer-based applications.</p>"

description2:
  enable: 1
  title: Project Result
  text1: "<p><b>PDF Object</b> simplifies the process of embedding PDF files in Polymer projects. It provides a customizable viewer component that supports features like card layout and elevation. The project has gained popularity for its ease of use and contribution to enhancing the PDF viewing experience in Polymer applications.</p>"

gallery:
  enable: 0
  images:
    - images/works/work5.jpeg
    - images/blog/blog8.jpg
    - images/blog/blog6.jpg

video:
  enable: 0
  poster: images/blog/blog9.jpg
  id: Gu6z6kIukgg

---
