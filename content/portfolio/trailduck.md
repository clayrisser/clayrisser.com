---
# preview details
title: TrailDuck
category: Development
category_slug: development
image: images/portfolio/trailduck.png
short_description: A topological cyclic sorting algorithm based on depth-first search (DFS).

# full details
live_preview: https://github.com/clayrisser/trailduck
full_image: images/portfolio/trailduck.png
info:
  - label: Year
    value: 2018-Present
  - label: Technology
    value: Nodejs

description1:
  enable: 1
  title: Project Goal
  text1: "<p>The <b>TrailDuck</b> project was created to provide a reliable algorithm for topological cyclic sorting based on depth-first search (DFS). It is designed to work with cycles in a graph and detect overlapping cycles. This project demonstrates my expertise in algorithm design and my ability to develop efficient solutions to complex problems.</p>"

gallery:
  enable: 0
  images:
    - images/works/work5.jpeg
    - images/blog/blog8.jpg
    - images/blog/blog6.jpg

description2:
  enable: 1
  title: Project Result
  text1: "<p>The <b>TrailDuck</b> project has been well-received by the developer community and has proven to be a valuable tool for sorting cyclic dependencies. Its implementation based on DFS provides a robust solution for handling complex graphs. By actively maintaining and improving this project, I continue to contribute to the advancement of algorithmic solutions in the Node.js ecosystem.</p>"

video:
  enable: 0
  poster: images/blog/blog9.jpg
  id: Gu6z6kIukgg
---
