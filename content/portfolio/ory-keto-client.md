---
# Preview Details
title: Ory Keto Client
category: JavaScript
category_slug: javascript
image: images/portfolio/ory-keto-client.png
short_description: Ory Keto access control client for JavaScript and TypeScript.

# Full Details
live_preview: https://github.com/clayrisser/ory-keto-client
full_image: images/portfolio/ory-keto-client.png
info:
  - label: Year
    value: 2018-Present
  - label: Technology
    value: JavaScript, TypeScript

description1:
  enable: 1
  title: Project Goal
  text1: "<p>The <b>Ory Keto Client</b> project provides a JavaScript and TypeScript client library for Ory Keto, an access control system. It enables developers to easily manage access control policies, including roles and custom policies, in their applications.</p>
  <p>This project demonstrates my expertise in JavaScript and TypeScript development and showcases my ability to integrate with external systems to enhance application security.</p>"

gallery:
  enable: 0
  images:
    - images/works/work5.jpeg
    - images/blog/blog8.jpg
    - images/blog/blog6.jpg

description2:
  enable: 1
  title: Project Result
  text1: "<p>The <b>Ory Keto Client</b> project has received positive feedback from the developer community and has been widely adopted for access control in JavaScript and TypeScript applications. It reflects my dedication to providing reliable and efficient tools that contribute to the overall security of web applications.</p>
  <p>By actively maintaining and improving this project, I continue to showcase my expertise in JavaScript and TypeScript development and my commitment to supporting the developer community.</p>"

video:
  enable: 0
  poster: images/blog/blog9.jpg
  id: Gu6z6kIukgg

---
