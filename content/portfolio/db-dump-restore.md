---
# preview details
title: DB Dump Restore
category: Database
category_slug: database
image: images/portfolio/db-dump-restore.png
short_description: Scripts for dumping and restoring data from databases.

# full details
live_preview: https://github.com/clayrisser/db-dump-restore
full_image: images/portfolio/db-dump-restore.png
info:
  - label: Year
    value: 2020-Present
  - label: Technology
    value: Postgres, MongoDB

description1:
  enable: 1
  title: Project Goal
  text1: "<p>The <b>DB Dump Restore</b> project aimed to provide convenient scripts for dumping and restoring data from databases. The project includes scripts for both Postgres and MongoDB, allowing users to easily backup and restore their data, whether it's a local database or one in a Kubernetes environment.</p>"

description2:
  enable: 1
  title: Project Result
  text1: "<p><b>DB Dump Restore</b> is a collection of scripts that simplify the process of database backup and restoration. It offers scripts for dumping entire databases or specific databases, as well as scripts for restoring both all databases and specific databases. The project has gained popularity for its user-friendly approach and its contribution to efficient data management workflows.</p>"

gallery:
  enable: 0
  images:
    - images/works/work5.jpeg
    - images/blog/blog8.jpg
    - images/blog/blog6.jpg

video:
  enable: 0
  poster: images/blog/blog9.jpg
  id: Gu6z6kIukgg
---
