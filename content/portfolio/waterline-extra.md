---
# preview details
title: Waterline Extra
category: JavaScript
category_slug: javascript
image: images/portfolio/waterline-extra.png
short_description: Extra methods for Waterline ORM, providing additional functionalities.

# full details
live_preview: https://github.com/clayrisser/waterline-extra
full_image: images/portfolio/waterline-extra.png
info:
  - label: Year
    value: 2018-Present
  - label: Technology
    value: Node.js, Waterline ORM

description1:
  enable: 1
  title: Project Goal
  text1: "<p>The <b>waterline-extra</b> project aims to extend the functionality of the Waterline ORM by providing additional methods. It seeks to enhance the development experience by offering convenient and useful utilities for working with Waterline.</p>"

description2:
  enable: 1
  title: Project Result
  text1: "<p><b>waterline-extra</b> introduces extra methods to Waterline, such as a promisified save method. These additional functionalities enable developers to streamline their code and enhance the capabilities of their Waterline-based applications. It has received positive feedback from the community for its contribution to the Waterline ecosystem.</p>"

gallery:
  enable: 0
  images:
    - images/works/work5.jpeg
    - images/blog/blog8.jpg
    - images/blog/blog6.jpg

video:
  enable: 0
  poster: images/blog/blog9.jpg
  id: Gu6z6kIukgg

---
