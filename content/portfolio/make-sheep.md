---
# preview details
title: Make Sheep
category: DevOps
category_slug: devops
image: images/portfolio/make-sheep.png
short_description: A development environment for AWS Lambda.

# full details
live_preview: https://github.com/clayrisser/make-sheep
full_image: images/portfolio/make-sheep.png
info:
  - label: Year
    value: 2017-Present
  - label: Technology
    value: Python

description1:
  enable: 1
  title: Project Goal
  text1: "<p>The <b>make-sheep</b> project aims to provide a development environment for AWS Lambda. It offers a virtual environment that enables developers to test and deploy their Lambda functions locally before deploying them to the AWS cloud.</p>"

description2:
  enable: 1
  title: Project Result
  text1: "<p><b>make-sheep</b> simplifies the development and deployment process for AWS Lambda functions. It provides a convenient way to set up a virtual environment, manage dependencies, and test Lambda functions locally. The project has gained popularity for its contribution to AWS Lambda development workflows.</p>"

gallery:
  enable: 0
  images:
    - images/works/work5.jpeg
    - images/blog/blog8.jpg
    - images/blog/blog6.jpg

video:
  enable: 0
  poster: images/blog/blog9.jpg
  id: Gu6z6kIukgg
---
