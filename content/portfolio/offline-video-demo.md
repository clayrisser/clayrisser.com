---
# preview details
title: Offline Video Demo
category: Node.js
category_slug: nodejs
image: images/portfolio/offline-video-demo.png
short_description: A Node.js application for recording and downloading offline videos using WebRTC.

# full details
live_preview: https://github.com/clayrisser/offline-video-demo
full_image: images/portfolio/offline-video-demo.png
info:
  - label: Year
    value: 2019-Present
  - label: Technology
    value: Node.js

description1:
  enable: 1
  title: Project Goal
  text1: "<p>The <b>offline-video-demo</b> project focuses on developing a Node.js application that enables users to record and download offline videos. It leverages WebRTC technology and provides a user-friendly interface for capturing video using the device's camera.</p>"

description2:
  enable: 1
  title: Project Result
  text1: "<p><b>offline-video-demo</b> empowers users to record and save videos for offline usage. By utilizing WebRTC and camcorder input, the application offers a seamless experience for capturing videos. The project has received positive feedback for its contribution to offline video recording capabilities.</p>"

gallery:
  enable: 0
  images:
    - images/works/work5.jpeg
    - images/blog/blog8.jpg
    - images/blog/blog6.jpg

video:
  enable: 0
  poster: images/blog/blog9.jpg
  id: Gu6z6kIukgg

---
