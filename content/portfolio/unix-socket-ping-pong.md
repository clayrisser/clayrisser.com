---
# preview details
title: Unix Socket Ping Pong
category: Development
category_slug: development
image: images/portfolio/unix-socket-ping-pong.png
short_description: Demonstration of a Node.js Unix socket.

# full details
live_preview: https://github.com/clayrisser/unix-socket-ping-pong
full_image: images/portfolio/unix-socket-ping-pong.png
info:
  - label: Year
    value: 2018-Present
  - label: Technology
    value: Node.js

description1:
  enable: 1
  title: Project Goal
  text1: "<p>The <b>Unix Socket Ping Pong</b> project aims to demonstrate the usage of Unix sockets in Node.js applications. It provides a simple ping-pong application utilizing Unix sockets for interprocess communication.</p>"

description2:
  enable: 1
  title: Project Result
  text1: "<p><b>Unix Socket Ping Pong</b> showcases the capabilities of Unix sockets in Node.js, enabling seamless interprocess communication. The project supports both Windows and Linux environments, making it versatile for various development setups. It has gained recognition for its contribution to understanding Unix socket usage in Node.js applications.</p>"

gallery:
  enable: 0
  images:
    - images/works/work5.jpeg
    - images/blog/blog8.jpg
    - images/blog/blog6.jpg

video:
  enable: 0
  poster: images/blog/blog9.jpg
  id: Gu6z6kIukgg

---
