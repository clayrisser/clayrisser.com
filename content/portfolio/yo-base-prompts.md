---
# preview details
title: Yo Base Prompts
category: Development
category_slug: development
image: images/portfolio/yo-base-prompts.png
short_description: Base prompts for Yeoman generators.

# full details
live_preview: https://github.com/clayrisser/yo-base-prompts
full_image: images/portfolio/yo-base-prompts.png
info:
  - label: Year
    value: 2018-Present
  - label: Technology
    value: Node.js

description1:
  enable: 1
  title: Project Goal
  text1: "<p>The <b>yo-base-prompts</b> project aims to provide a set of base prompts for Yeoman generators. It offers a collection of prompts that can be used as a foundation for creating custom Yeoman generators with standardized user interactions.</p>"

description2:
  enable: 1
  title: Project Result
  text1: "<p><b>yo-base-prompts</b> provides essential prompts for Yeoman generators. By utilizing these base prompts, developers can create consistent and user-friendly interactions when generating projects with Yeoman. The project has been well-received for its contribution to enhancing the user experience of Yeoman-based workflows.</p>"

gallery:
  enable: 0
  images:
    - images/works/work5.jpeg
    - images/blog/blog8.jpg
    - images/blog/blog6.jpg

video:
  enable: 0
  poster: images/blog/blog9.jpg
  id: Gu6z6kIukgg

---
