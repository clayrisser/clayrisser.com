---
# preview details
title: Math Playground
category: Configuration
category_slug: configuration
image: images/portfolio/math-playground.png
short_description: A personal math playground.

# full details
live_preview: https://github.com/clayrisser/math-playground
full_image: images/portfolio/math-playground.png
info:
  - label: Year
    value: 2017-Present
  - label: Technology
    value: Python

description1:
  enable: 1
  title: Project Goal
  text1: "<p>The <b>Math Playground</b> project is a personal math playground that provides various mathematical features. It offers a collection of tools and functionalities for graphing and exploring mathematical concepts.</p>"

description2:
  enable: 1
  title: Project Result
  text1: "<p>The <b>Math Playground</b> serves as a personal math exploration and experimentation platform. It includes features for graphing and provides an environment for testing mathematical functions and concepts. The project has been useful for personal mathematical exploration and experimentation.</p>"

gallery:
  enable: 0
  images:
    - images/works/work5.jpeg
    - images/blog/blog8.jpg
    - images/blog/blog6.jpg

video:
  enable: 0
  poster: images/blog/blog9.jpg
  id: Gu6z6kIukgg

---
