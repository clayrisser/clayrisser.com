---
title: "1000 ways to npm clean"
date: 2019-12-03T12:49:27+06:00
featureImage: images/blog/1000-ways-to-npm-clean.png
tags: []
categories:
  - JavaScript
---

Ok, well there's not really 1000 ways to npm clean, but it certainly feels like it. Below is my journey figuring out the best approach. And yes, I finally stumbled upon [**the holy grail**](#git-clean) of npm clean.

> SPOILER ALERT: Skip to the last approach if you don't have the patience to read through all of this.

# rm -r

Yep, classic Unix commands are probably the easiest way to create an npm clean script. This was my goto for quite a while because it just made sense coming from a Unix world.

It has drawbacks though . . . **windows != Unix**, so don't expect it to work on windows.

_package.json_

```json
{
  "scripts": {
    "clean": "rm -rf .cache .tmp lib build"
  }
}
```

# rimraf

So, the solution to a cross-platform npm clean script is [rimraf](https://www.npmjs.com/package/rimraf). [rimraf](https://www.npmjs.com/package/rimraf) was bulit by the creator of npm in 2011 (back in the day when NodeJS was just beginning to gain traction). It works almost like _rm -r_.

I guess the only drawback is that you have to install it as a dev dependency.

_package.json_

```json
{
  "scripts": {
    "clean": "rimraf .cache .tmp lib build"
  },
  "devDependencies": {
    "rimraf": "^2.6.2"
  }
}
```

# clean script

Sometimes your build is so complex you need to describe the process in JavaScript. In this case, building a designated script for cleaning makes sense. If you build it right, you can simply add it to the _package.json_ clean script.

This approach certainly has the drawback of requiring a lot of code for a simple task.

_package.json_

```json
{
  "scripts": {
    "build": "node tools/build.js",
    "clean": "node tools/clean.js"
  },
  "devDependencies": {
    "fs-extra": "^7.0.1"
  }
}
```

_tools/clean.js_

```js
import fs from "fs-extra";

import path from "path";
async function main() {
  await clean();
}

export default async function clean() {
  await fs.remove(path.resolve(__dirname, "../.cache"));
  await fs.remove(path.resolve(__dirname, "../.tmp"));
  await fs.remove(path.resolve(__dirname, "../lib"));
  await fs.remove(path.resolve(__dirname, "../build"));
}

if (typeof require !== "undefined" && require.main === module) {
  main();
}
```

_tools/build.js_

```js
import clean from "./clean";
async function main() {
  await clean();
  await build();
}

export default async function build() {
  console.log("building something special . . .");
}

if (typeof require !== "undefined" && require.main === module) {
  main();
}
```

# git clean

So . . . I did promise the **holy grail of npm clean**. It is probably safe to assume your node project is in a git repo. If it is, why not use git to clean your repo. I stumbled upon this fairly recently, and it is amazing. The _.gitignore_ file already tells me everything I don't want in my repo.

The following command will remove all files from your git repo that are in your _.gitignore_ file.

```sh
git clean -fxd
```

However, when I clean my repo, I don't want to remove all my dependencies in the process. Simply adding _!node_modules_ and \*!node_modules/**/** to the exclude flag will keep that folder. It's a double negative. You're basically saying **do not exclude node_modules**. Make sure you escape the bang so bash can understand it.

```sh
git clean -fxd -e \!node_modules -e \!node_modules/**/*
```

When adding it to our _package.json_ clean script, we have to escape the escape symbol so JSON can parse it, hence it becomes _\\!node_modules_ and \*\\!node_modules/**/**.

The only drawback to this approach is it does not work unless you are in a git repo.

_package.json_

```json
{
  "scripts": {
    "clean": "git clean -fxd -e \\!node_modules -e \\!node_modules/**/*"
  }
}
```

If you have used other approaches for cleaning npm repositories, please let me know in the comments.
