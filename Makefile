HUGO ?= hugo

.PHONY: dev
dev:
	@$(HUGO) server

.PHONY: build
build:
	@$(HUGO)
